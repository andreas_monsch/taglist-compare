# How to create a taglist comparison report?

In order to create a taglist comparison report for a model with different revisions, run them `taglist_compare by-git-tags` command, e.g.

```bash
# comparison command
PYTHONPATH=. python taglist_compare by-git-tags \
# this argument refers to the git repository of the machine project
/home/amo/workspace/rommelag/MachineProjects/434004 \
# these arguments specify which two tags should be used for comparison
RppModel_V1_5 RppModel_V1_8
```
