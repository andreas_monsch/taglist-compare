# pylint: disable=missing-module-docstring,missing-function-docstring

import csv
import logging

import yaml
from pyunits.units import cefact_units


logger = logging.getLogger(__name__)


def mangle_name(name):
    return name.replace(".", "_")


def _recurse(ns_index, node, path, node_infos):
    node_name = mangle_name(node["name"])
    node_path = f"{path}.{node_name}"

    for mapping in node.get("mappings", []):
        mapping_name = mangle_name(mapping["name"])
        mapping_path = f"{node_path}.{mapping_name}"
        mapping["path"] = mapping_path
        mapping["nodeId"] = f"ns={ns_index};i={mapping['nodeId']}"
        if "unit" in mapping and mapping["unit"]:
            mapping["unit"] = cefact_units[mapping["unit"]][1]
        node_infos.append(mapping)

    for child_node in node.get("nodes", []):
        _recurse(ns_index, child_node, node_path, node_infos)


def walk_model(model):
    ns_index = model["nsIdx"]

    node_infos = []
    for toplevel_node in model["nodes"]:
        node_name = mangle_name(model["name"])
        _recurse(ns_index, toplevel_node, f"Objects.{node_name}", node_infos)
    return node_infos


def generate_kepware(node_infos, filename):
    with open(filename, "w+") as fil:
        writer = csv.DictWriter(
            fil,
            [
                "Tag Name",
                "Address",
                "Data Type",
                "Respect Data Type",
                "Client Access",
                "Scan Rate",
                "Scaling",
                "Raw Low",
                "Raw High",
                "Scaled Low",
                "Scaled High",
                "Scaled Data Type",
                "Clamp Low",
                "Clamp High",
                "Eng Units",
                "Description",
                "Negate Value",
            ],
        )

        writer.writeheader()

        for node in node_infos:
            row = {
                "Tag Name": node["path"],
                "Address": node["nodeId"],
                "Data Type": "Default",
                "Respect Data Type": 1,
                "Client Access": "R/W" if node["writable"] else "RO",
                "Eng Units": node.get("unit", ""),
            }
            writer.writerow(row)


def _data_type(node):
    if "dtype" not in node and node["type"] == "cond":
        return "Boolean"
    if node["dtype"] == "int":
        return "Int32"
    if node["dtype"] == "float":
        return "Float"
    if node["dtype"] == "bool":
        return "Boolean"
    if node["dtype"] == "str":
        return "String"
    return "Unknown"


def _rpp_type(node):
    if node["type"] == "pval":
        return "Processvalue"
    if node["type"] == "stpt":
        return "Setpoint"
    if node["type"] == "cond":
        return "Condition/Alarm"
    return "Unknown"


def _eu_range(node):
    if "rangeMax" in node and "rangeMin" in node:
        return f"{float(node['rangeMin'])} - {float(node['rangeMax'])}"
    return ""


def generate_view(node_infos, filename, version):
    with open(filename, "w+") as fil:
        fil.write("Version: %s\n" % version)
        writer = csv.DictWriter(
            fil,
            [
                "OPCUA NodeID",
                "OPCUA Browsepath",
                "RPP Type",
                "OPCUA Datatype",
                "Engineering Unit",
                "Engineering Range",
                "Process Critical",
                "Manufacturer Code",
                "Writable",
            ],
        )
        writer.writeheader()

        for node in node_infos:
            row = {
                "OPCUA NodeID": node["nodeId"],
                "OPCUA Browsepath": node["path"],
                "RPP Type": _rpp_type(node),
                "OPCUA Datatype": _data_type(node),
                "Engineering Unit": node.get("unit", ""),
                "Engineering Range": _eu_range(node),
                "Process Critical": node["critical"],
                "Manufacturer Code": node.get("code", ""),
                "Writable": node["writable"],
            }
            writer.writerow(row)


def create(model_file):
    machine_number = None
    version = None

    node_infos = []
    logger.info("Reading file: %s", model_file)
    model = yaml.safe_load(model_file)

    logger.info("Walking model: %s", model_file)
    node_infos.extend(walk_model(model))

    if not machine_number and not version:
        machine_number = model["name"]
        version = model["nsVersion"]

    logger.info("Generating view CSV")
    filename_view = "%s-taglist-view-%s.csv" % (machine_number, version)
    generate_view(node_infos, filename_view, version)
    return filename_view
