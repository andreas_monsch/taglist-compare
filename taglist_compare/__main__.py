#!/usr/bin/env python

"""
Created on Wed May 26 17:09:09 2021

@author: Philipp Bayer et al.
"""

# TODOs:
#        1. Fix strange character stuff in the input taglists
#        2. etc.

import csv
import io
import json
import os
from os.path import splitext, basename
import re
import sys

import click
import git

from openpyxl import Workbook
from openpyxl.styles import PatternFill

from taglist_compare.taglist_create import create


def _read_taglist(source_file, delimiter=","):
    version = source_file.readline().strip()  # drop version info from CSV
    taglist = []
    reader = csv.DictReader(source_file, delimiter=delimiter)
    for row in reader:
        namespace, node_id = row["OPCUA NodeID"].split(";")
        row["OPCUA NodeID"] = node_id.split("=")[1]
        if "GMP Critical" in row:
            row["Process Critical"] = row.pop("GMP Critical")
        taglist.append(row)

    return {
        "namespace": namespace,
        "version": version,
        "header": reader.fieldnames,
        "rows": sorted(taglist, key=lambda row: row["OPCUA NodeID"]),
    }


def _pad_taglist(taglist, missing):
    rows = taglist["rows"]
    keys = rows[0].keys()
    for nid in missing:
        rows.append(
            {
                **{key: None for key in keys},
                **{
                    "OPCUA NodeID": nid,
                },
            }
        )

    rows = sorted(rows, key=lambda row: row["OPCUA NodeID"])
    taglist["rows"] = rows
    return taglist


def _write_excel_file(sources, taglists, output_filename, annotation_color="0000B0F0"):
    # Export to .xlsx
    workbook = Workbook()
    left_sheet = workbook.active
    left_sheet.title = basename(splitext(sources[0])[0])
    right_sheet = workbook.create_sheet(basename(splitext(sources[1])[0]))

    left, right = taglists

    # Write the header into boot sheets
    for i, field in enumerate(taglists[0]["header"], 1):
        left_sheet.cell(1, i).value = field
        right_sheet.cell(1, i).value = field

    fill = PatternFill(fill_type="darkDown", fgColor=annotation_color)

    for i, row in enumerate(left["rows"], 1):
        left_values = set(row.values())
        right_row = right["rows"][i - 1]
        right_values = set(right_row.values())
        for j, (key, value) in enumerate(row.items()):
            left_sheet.cell(i + 1, j + 1).value = value
            if len(left_values) == 2:
                left_sheet.cell(i + 1, j + 1).fill = fill

            right_sheet.cell(i + 1, j + 1).value = right_row.get(key)
            if len(right_values) == 2:
                right_sheet.cell(i + 1, j + 1).fill = fill

    workbook.save(output_filename)


@click.group()
@click.pass_context
def compare(ctx):
    pass


@compare.command()
@click.argument("sources", nargs=2, type=click.File(), required=True)
@click.option("--output-filename", default=None, required=False)
def by_files(sources, output_filename):
    """ Compare to tagists and create an Excel File"""
    _compare(sources, output_filename)


def _compare(sources, output_filename=None):
    """ Compare to tagists and create an Excel File"""
    if output_filename is None:
        output_filename = f"{basename(splitext(sources[0].name)[0])}_CHANGE.xlsx"

    taglists = [_read_taglist(source) for source in sources]
    namespaces = list({taglist["namespace"] for taglist in taglists})
    if len(namespaces) != 1:
        click.echo(
            click.style("[WARNING]: ", fg="red")
            + f"The namespace index has changed! {namespaces[0]} <-> {namespaces[1]}"
        )

    nids_left = [row["OPCUA NodeID"] for row in taglists[0]["rows"]]
    nids_right = [row["OPCUA NodeID"] for row in taglists[1]["rows"]]
    missing_right = set(nids_left).difference(nids_right)
    missing_left = set(nids_right).difference(nids_left)

    taglists[0] = _pad_taglist(taglists[0], missing_left)
    taglists[1] = _pad_taglist(taglists[1], missing_right)

    _write_excel_file([sources[0].name, sources[1].name], taglists, output_filename)


def _find_model_file(tree, model_pattern="BP.*\.yaml"):
    matches = [
        item for item in tree.list_traverse() if re.search(model_pattern, item.name)
    ]
    if len(matches) > 1:
        click.echo(
            click.style("[Error]", fg="red") + " More than one model file found!"
        )
        click.echo(f"Candidate: {matches}")
        sys.exit(1)

    if len(matches) == 0:
        click.echo(click.style("[Error]", fg="red") + " No model file(s) found!")
        sys.exit(1)

    return matches[0]


@compare.command()
@click.argument("tags", nargs=2)
@click.option("--repository", type=click.Path(), default=".")
@click.option("--output-filename", default=None, required=False)
def by_git_tags(
    repository,
    tags,
    output_filename,
):
    repo = git.Repo(repository)
    available_tags = {tag.name: tag for tag in repo.tags}
    if len(set(tags).intersection(available_tags)) != 2:
        print(
            f"Invalid tag specified - valid tags are: {json.dumps(list(available_tags.keys()), indent=2)}"
        )
        return

    left_tag = available_tags[tags[0]]
    click.echo(f"Reading model file with {left_tag} revision")
    left_model_bytes = io.BytesIO(
        _find_model_file(left_tag.commit.tree).data_stream.read()
    )

    right_tag = available_tags[tags[1]]
    click.echo(f"Reading model file with {right_tag} revision")
    right_model_bytes = io.BytesIO(
        _find_model_file(right_tag.commit.tree).data_stream.read()
    )

    click.echo(f"Creating taglist for {left_tag} revision")
    left_source = create(left_model_bytes)
    click.echo(f"-> {left_source}")

    click.echo(f"Creating taglist for {right_tag} revision")
    right_source = create(right_model_bytes)
    click.echo(f"-> {right_source}")

    click.echo("Comparing taglists ...")
    with open(left_source) as left_source_handle, open(
        right_source
    ) as right_source_handle:
        _compare([left_source_handle, right_source_handle])

    click.echo("Cleaning up taglists ...")
    os.remove(left_source)
    os.remove(right_source)


if __name__ == "__main__":
    compare()  # pylint: disable no-value-for-parameter
