# pylint: disable=missing-module-docstring

import setuptools


def read_file(file_name):
    """Get trimmed text of a file with a path relative to the module"""
    with open(file_name) as fil:
        return fil.read().strip()


def install_requires():
    """ Return requirements as list from requirements.txt """
    with open("requirements.txt") as reqs:
        return [dep.split()[0] for dep in reqs if "==" in dep]


setuptools.setup(
    name="taglist-compare",
    version=f"1.0.0",
    author="Rommelag iLabs",
    author_email="ikreis@rommelag.com",
    packages=setuptools.find_packages(),
    install_requires=install_requires(),
    entry_points={"console_scripts": ["taglist-compare = taglist_compare.__main__:compare"]},
)
